console.log('from ts')

import { shuffle } from '@beenotung/tslib/array'
import { patterns } from './patterns'

let { log, random, floor } = Math

let isRunning = false

let W = 32
let H = 32

let N = W * H

let canvas = document.querySelector('canvas#input') as HTMLCanvasElement

let context = canvas.getContext('2d')!

canvas.width = W
canvas.height = H

function init() {
  let image = context.getImageData(0, 0, W, H)
  for (let y = 0; y < H; y++) {
    for (let x = 0; x < W; x++) {
      let i = (y * W + x) * 4
      image.data[i + 0] = 255
      image.data[i + 1] = 255
      image.data[i + 2] = 255
      image.data[i + 3] = 255
    }
  }
  context.putImageData(image, 0, 0)
}
init()

let isPenDown = false
let drawMode: null | 'black' | 'white' = null
function penDown(event: { clientX: number; clientY: number }) {
  let rect = canvas.getBoundingClientRect()
  let x = floor(((event.clientX - rect.left) / rect.width) * W)
  let y = floor(((event.clientY - rect.top) / rect.height) * H)

  let i = (y * W + x) * 4
  let image = context.getImageData(0, 0, W, H)
  let r = image.data[i + 0]
  if (drawMode == null) {
    drawMode = r == 0 ? 'white' : 'black'
  }
  context.fillStyle = drawMode
  context.fillRect(x, y, 1, 1)
}
canvas.addEventListener('mousedown', event => {
  isPenDown = true
  drawMode = null
  penDown(event)
})
canvas.addEventListener('mousemove', event => {
  if (isPenDown) {
    penDown(event)
  }
})
canvas.addEventListener('mouseup', event => {
  isPenDown = false
})

function exportPattern() {
  let image = context.getImageData(0, 0, W, H)
  let pattern = []
  for (let y = 0; y < H; y++) {
    let line = []
    for (let x = 0; x < W; x++) {
      let i = (y * W + x) * 4
      let r = image.data[i + 0]
      line.push(r === 0 ? 0 : 1)
    }
    pattern.push(line)
  }
  console.log(JSON.stringify(pattern))
  return pattern
}

// delete patterns['1']
// delete patterns['7']
// delete patterns['3']
// delete patterns['8']

function restorePattern(name: string) {
  let image = context.getImageData(0, 0, W, H)
  let i = 0
  let pattern = patterns[name]
  pattern.forEach(line =>
    line.forEach(cell => {
      let r = cell === 1 ? 255 : 0
      image.data[i + 0] = r
      image.data[i + 1] = r
      image.data[i + 2] = r
      image.data[i + 3] = 255
      i += 4
    }),
  )
  context.putImageData(image, 0, 0)
}

function getPatternVector(name: string) {
  let pattern = patterns[name]
  let vector: number[] = []
  pattern.forEach(line =>
    line.forEach(cell => {
      vector.push(cell > 0 ? 1 : -1)
    }),
  )
  return vector
}

function getAllPatternVector() {
  return Object.keys(patterns).map(name => getPatternVector(name))
}

class HopFieldNN {
  private memoryList: number[][] = []
  private N = 0
  private neurons: number[] = []
  private neuron_index_list: number[] = []

  private readonly n = 2

  setMemoryList(memoryList: number[][]) {
    this.memoryList = memoryList
    this.N = memoryList[0].length
    this.neurons = new Array(this.N).fill(0)
    this.neuron_index_list = new Array(this.N).fill(0).map((_, i) => i)
  }

  get neuronVector() {
    return this.neurons
  }

  update(max_step = 1000): number {
    for (let step = 0; step < max_step; step++) {
      this.neuron_index_list = shuffle(this.neuron_index_list)
      let changed = 0
      for (let i of this.neuron_index_list) {
        let curEnergy = this.energy(this.neurons)

        this.neurons[i] *= -1
        let invEnergy = this.energy(this.neurons)

        if (curEnergy <= invEnergy) {
          this.neurons[i] *= -1
        } else {
          changed++
        }
      }
      if (changed === 0) {
        console.log('converged')
        return step
      }
    }
    return max_step
  }

  private energy(neuronVector: number[]) {
    let E = 0
    this.memoryList.forEach(memoryVector => {
      for (let i = 0; i < this.N; i++) {
        let x = neuronVector[i] * memoryVector[i]
        x = x < 0 ? 0 : x
        // F(x^a)  (this.n is called as a in the 2017 paper)
        x = x ** this.n
        E -= x
      }
    })
    return E
  }
}

function startNN() {
  isRunning = true

  let image = context.getImageData(0, 0, W, H)

  let nn = new HopFieldNN()
  nn.setMemoryList(getAllPatternVector())

  let neurons = nn.neuronVector
  let pattern = exportPattern()
  let i = 0
  pattern.forEach(line => {
    line.forEach(cell => {
      neurons[i] = cell > 0 ? 1 : -1
      i++
    })
  })

  loopNN(image, 1, nn)
}
function stopNN() {
  isRunning = false
}

function loopNN(image: ImageData, round: number, nn: HopFieldNN) {
  if (!isRunning) return
  console.log('round:', round)

  let step = nn.update(1)

  render(image, nn)

  if (step == 0) {
    isRunning = false
    return
  }

  // setTimeout(() => loopNN(image, round + 1, nn), 30)
  requestAnimationFrame(() => loopNN(image, round + 1, nn))
}

function render(image: ImageData, nn: HopFieldNN) {
  let xs = nn.neuronVector
  let x_i = 0
  for (let y = 0; y < H; y++) {
    for (let x = 0; x < W; x++) {
      let value = xs[x_i]
      x_i++
      /* 
			map +1 -> 255
			map -1 -> 0
			*/
      let r = ((value + 1) / 2) * 255

      let i = (y * W + x) * 4
      image.data[i + 0] = r
      image.data[i + 1] = r
      image.data[i + 2] = r
      image.data[i + 3] = 255
    }
  }
  context.putImageData(image, 0, 0)
}

Object.assign(window, {
  exportPattern,
  startNN,
  stopNN,
  restorePattern,
  patterns,
})
